import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter as Router, Route} from 'react-router-dom';

import Home from "./feature/Home/components/Home";

const routing = (
  <Router>
        <Route exact component={Home} path="/" />
  </Router>
);

ReactDOM.render(routing, document.getElementById('root'));